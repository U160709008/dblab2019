create view usa_customers as 
select CustomerID, CustomerName, ContactName
from Customers
where Country * "USA"; 

select * from usa_customers;

select *
from usa_customers join orders on usa_customers.CustomerID = orders.CustomerID;

select avg(Price) from Products;

create view products_below_avg_price as 
select ProductID, ProductName, Price
from Products
where Price < 30;
#where Price < (select avg(Price) from Products);

#select * from products_below_avg_price;

select *
from usa_customers join Orders on usa_customers.CustomerID = Orders.CustomerID
where OrderID in (
	select OrderID
    from OrderDetails join products_below_avg_price on OrderDetails.ProductID = products_below_avg_price.ProductID
);

drop view usa_customers;